import XCTest
@testable import helloTests

XCTMain([
     testCase(helloTests.allTests),
])
