import PackageDescription

let package = Package(
    name: "hello",
    targets: [
    	Target(
            name: "Server",
            dependencies: [.Target(name: "TodoList")]
        ),
    	Target(
            name: "TodoList"
        )
    ],
   dependencies: [
        .Package(url: "https://github.com/IBM-Swift/Kitura.git", majorVersion: 1),
        .Package(url: "https://github.com/IBM-Swift/LoggerAPI.git", majorVersion: 1),
        .Package(url: "https://github.com/IBM-Swift/HeliumLogger.git", majorVersion: 1)
     ]
)
