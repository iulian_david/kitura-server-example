//
//  SuperTodoList.swift
//  hello
//
//  Created by iulian david on 2/19/17.
//
//

import Kitura
import LoggerAPI
import SwiftyJSON
import Dispatch
import Foundation

public class SuperTodoList {
    
    public let router = Router()
    
    let queue = DispatchQueue(label: "com.example.todo")
    
    public init() {
        ///Kitura has a piece of middleware called BodyParser that will parse
        ///the body of the request before sending it to the handler. BodyParser
        ///reads the Content-Type of a message, and fills in the body field of
        ///the RouterRequest with a ParsedBody enumeration.
        ///The enumeration includes cases for json, text, raw, and other values. But for the
        ///BodyParser to work, you must register it on routes that need this
        ///payload information
        router.all("/*", middleware: BodyParser())
        
        router.get("/hello") {
            request, response, next in
            let name = request.queryParameters["name"] ?? "World!"
            response.send( json: JSON.parse(string: "[\"Hello, \(name)\"]") )
            next()
        }
        
        router.get("/hello/:name") {
            request, response, callNextHandler in
            guard let name = request.parameters["name"]
                else {
                    response.status(.badRequest)
                    callNextHandler()
                    return
            }
            response.status(.OK).send( json: JSON.parse(string: "[\"Hello, King \(name)\"]"))
            callNextHandler()
        }
        
        
        
        // First Act
        addRoutesForStringItems(router: router)
        
        // Second Act
        addRoutesForDictItems(router: router)
        
        // Third Act
        addRoutesForStructItems(router: router)
    }
}
