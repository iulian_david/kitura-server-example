//
//  TodoListItem_String.swift
//  hello
//
//  Created by iulian david on 2/19/17.
//
//

import Foundation
import Dispatch
import Kitura
import SwiftyJSON


///The Array of items
var itemStrings = [String]()

///In Kitura, unlike NodeJS, multiple handlers run concurrently on different threads.
///To avoid data loss and potential heap corruption,
///serialize mutations to the itemStrings array with a statically allocated
//semaphore:
let itemStringsLock = DispatchSemaphore(value: 1)


///The GET handler
func handleGetStringItems(
    request: RouterRequest,
    response: RouterResponse,
    callNextHandler: @escaping () -> Void
    ) throws {
    response.send( json: JSON(itemStrings) )
    callNextHandler()
}


///The ADD handler
///Acts as POST handler
func handleAddStringItem(
    request: RouterRequest,
    response: RouterResponse,
    callNextHandler: @escaping () -> Void
    ) {
    // If there is a body that holds JSON, get it.
    guard case let .json(jsonBody)? = request.body
        else {
            response.status(.badRequest)
            callNextHandler()
            return
    }
    let item = jsonBody["item"].stringValue
    
    itemStringsLock.wait()
    itemStrings.append(item)
    itemStringsLock.signal()
    
    response.send("Added '\(item)'\n")
    callNextHandler()
}

func addRoutesForStringItems(router: Router) {
    router.get ("/v1/string/item", handler: handleGetStringItems)
    router.post("/v1/string/item", handler: handleAddStringItem)
}
