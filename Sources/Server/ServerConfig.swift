//
//  ServerConfig.swift
//  hello
//
//  Created by iulian david on 2/20/17.
//
//

import Foundation
import Kitura
import LoggerAPI





///Let's Encrypt stores the certificates in /etc/letsencrypt/live/[server_name]/
class ServerConfig {
    
    ///The server name
    
    let serverName=""
    
    
    let fileManager = FileManager.default

    ///
    ///If one of the certificates is found than it builds a **SSLConfig** instance
    ///
    ///	- Returns:	New SSLConfig instance or nil.
    func getSSLConfig() -> SSLConfig? {
        
        ///SSL certificates
        let myCertPath = "/etc/letsencrypt/live/\(serverName)/cert.pem"
        let myKeyPath = "/etc/letsencrypt/live/\(serverName)/privkey.pem"
        let myFullChainPath = "/etc/letsencrypt/live/\(serverName)/fullchain.pem"
        
        //test if the files are found
        Log.verbose("chainPath \(myFullChainPath)")
        Log.verbose("Exists? \(fileManager.fileExists(atPath: myFullChainPath))")
        
        var mySSLConfig:SSLConfig?
        if fileManager.fileExists(atPath: myFullChainPath) {
            //if needed to use a SelfSigned Certificate it would be: SSLConfig(withCACertificateFilePath: nil, usingCertificateFile: myCertPath, withKeyFile: myKeyPath, usingSelfSignedCerts: true)
            mySSLConfig  =  SSLConfig(withChainFilePath: myFullChainPath, withPassword: myCertPath, usingSelfSignedCerts: false, cipherSuite: myKeyPath)
        } else {
            Log.verbose("starting without SSL")
        }

        return mySSLConfig
    }
}
