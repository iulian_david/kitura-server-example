import Foundation
import Kitura
import SwiftyJSON
import HeliumLogger
import LoggerAPI
import TodoList


// Attach a logger
Log.logger = HeliumLogger()

let superTodoList = SuperTodoList()

let serverConfig = ServerConfig()
if let mySSLConfig = serverConfig.getSSLConfig() {
    //if needed to use a SelfSigned Certificate it would be: SSLConfig(withCACertificateFilePath: nil, usingCertificateFile: myCertPath, withKeyFile: myKeyPath, usingSelfSignedCerts: true)
    Kitura.addHTTPServer(onPort: 8090, with: superTodoList.router, withSSL: mySSLConfig)
} else {
    Kitura.addHTTPServer(onPort: 8090, with: superTodoList.router)
}



Kitura.run()
